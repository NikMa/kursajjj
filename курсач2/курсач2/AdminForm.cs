﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace курсач2
{

    public partial class AdminForm : Form
    {
        string connectString = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = '|DataDirectory|Database1.mdf'; Integrated Security = True";
        string query;

        SqlConnection connection;
        SqlDataReader dataReader;

        Form form1;

        public AdminForm()
        {
            InitializeComponent();
        }

        public AdminForm(Form form)
        {
            InitializeComponent();
            form.Hide();
            form1 = form;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            form1.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Connect();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void Connect()
        {
            query = "SELECT * FROM Users";
            connection = new SqlConnection(connectString);
            connection.Open();
            dataReader = null;

            SqlCommand cmd = new SqlCommand(query, connection);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                int rows = dataGridView1.Rows.Add();

                dataGridView1.Rows[rows].Cells[0].Value = dataReader[0];
                dataGridView1.Rows[rows].Cells[1].Value = dataReader[1];
                dataGridView1.Rows[rows].Cells[2].Value = dataReader[3];
                dataGridView1.Rows[rows].Cells[3].Value = dataReader[4];
                dataGridView1.Rows[rows].Cells[4].Value = dataReader[5];
                dataGridView1.Rows[rows].Cells[5].Value = dataReader[6];
                dataGridView1.Rows[rows].Cells[6].Value = dataReader[7];
            }

            if (dataReader != null)
            {
                dataReader.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            form1.Close();
    
        }

        private void AdminForm_MouseDown(object sender, MouseEventArgs e)
        {
            Capture = false;
            Message m = Message.Create(Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            WndProc(ref m);
        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}
