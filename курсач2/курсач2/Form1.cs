﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace курсач2
{
    public partial class Form1 : Form
    {
        Form form1;

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(Form form)
        {
            InitializeComponent();
            form.Hide();
            form1 = form;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            form1.Close();
        }
    }
}
