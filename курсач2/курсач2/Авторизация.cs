﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace курсач2
{
   public partial class Авторизация : Form
    {
        string connectionString = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = '|DataDirectory|Database1.mdf'; Integrated Security = True";
        SqlConnection connection;
        SqlDataReader dataReader;

        public Авторизация()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Авторизация_MouseDown(object sender, MouseEventArgs e)
        {
            Capture = false;
            Message m = Message.Create(Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            WndProc(ref m);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            
            string quere = $"Select * from Users where login = '{textBox1.Text}' and password = '{textBox2.Text}'";
           SqlCommand cmd = new SqlCommand(quere, connection);
            dataReader = cmd.ExecuteReader();

            dataReader.Read();

            string test = dataReader[2].ToString();

            if (dataReader.HasRows && dataReader[2].ToString() == "1" )
            {
                dataReader.Close();
                AdminForm form1 = new AdminForm(this);
                form1.Show();

            }
            else if (dataReader.HasRows)
            {
                dataReader.Close();
                ClientForm form2 = new ClientForm(this);
                form2.Show();
            }
            else 
            {
                textBox1.ForeColor = Color.Red;
                textBox2.ForeColor = Color.Red;
                textBox1.Text = "Неверно";
                textBox2.Text = "Неверно";
            }
        }

        private void TextBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Focused && (textBox1.Text == "Login" || textBox1.Text == "Неверно"))
            {
                textBox1.ForeColor = Color.Black;
                textBox1.Clear();
            }
            if (textBox2.Focused && (textBox2.Text == "Password" || textBox2.Text == "Неверно"))
            {
                textBox2.ForeColor = Color.Black;
                textBox2.Clear();
            }

        }

        private void TextBox1_Leave(object sender, EventArgs e)
        {
            if (!textBox1.Focused && textBox1.Text == "")
            {
                textBox1.ForeColor = Color.Black;
                textBox1.Text = "Login";
            }
            if (!textBox2.Focused && textBox2.Text == "")
            {
                textBox2.ForeColor = Color.Black;
                textBox2.Text = "Password";
            }

        }
    }
}
